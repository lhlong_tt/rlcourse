##rlcourse

###Reinforcement Learning Resources

####Online Course:
- [David Silver’s course on Youtube](https://www.youtube.com/watch?v=2pWv7GOvuf0&list=PL7-jPKtc4r78-wCZcQn5IqyuWhBZ8fOxT&index=1): this course provide the based-knowledge of RL, need a lot of maths. You can see [the noted](https://gitlab.com/lhlong_tt/rlcourse/blob/master/RL%20Notes.pdf)
- [Deep RL Bootcamp](https://sites.google.com/view/deep-rl-bootcamp/lectures)
- [CS234](http://web.stanford.edu/class/cs234/project.html)

####Blog
- https://medium.freecodecamp.org/an-introduction-to-reinforcement-learning-4339519de419
- [Karpathy blog](http://karpathy.github.io/2016/05/31/rl/)



####Ebook
- [RL ebook 2018](https://gitlab.com/lhlong_tt/rlcourse/blob/master/RLbook2018.pdf)
- [RL algorithms for MDP](https://gitlab.com/lhlong_tt/rlcourse/blob/master/Reinforcement%20Learning%20Algorithms%20for%20MDPs.pdf)

####Git
- https://github.com/Pulkit-Khandelwal/Reinforcement-Learning-Notebooks


